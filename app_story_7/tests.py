from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_todo
from .models import NewStatus
from .forms import Form
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
import time

# class test_error(TestCase):
#     def test_first(self):
#         self.assertEqual(0,1)

class Story7UnitTest(TestCase):
    def test_story7_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story7_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_model_can_create_new_todo(self):
        new_activity = NewStatus.objects.create(title='Story 7', status='About Confirmation Page')
        counting_all_available_todo = NewStatus.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)
    
    def test_form_validation_for_blank_items(self):
        form = Form(data={'title': '', 'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'], ["This field is required."])

    def test_story_7_confirm_page_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

class Story7FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()
        
    def tearDown(self):
        self.selenium.quit()
        super(Story7FunctionalTest, self).tearDown()

    def test_input_new_status(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        time.sleep(10)

        try:
            title = selenium.find_element_by_id('title')
            status = selenium.find_element_by_id('status')
            submit = selenium.find_element_by_id('submit')
            username = "User"

            title.send_keys(username)
            title.implicitly_wait(10)
            
            status.send_keys('Selenium and Confirmation Page')
            status.implicitly_wait(10)

            submit.send_keys(Keys.RETURN)
            submit.implicitly_wait(10)

            submit2 = selenium.find_element_by_id('submit2')
            submit2.send_keys(Keys.RETURN)

            submit2.implicitly_wait(10)
            page = selenium.page_source
            
            self.assertIn(username, page)

        except NoSuchElementException:
            pass

    def test_input_new_status_cancelled(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        time.sleep(10)

        try:
            title = selenium.find_element_by_id("title")
            status = selenium.find_element_by_id('status')
            submit = selenium.find_element_by_id('submit')
            username = "User_2"

            title.send_keys(username)
            status.send_keys('Selenium and Confirmation Page')
            submit.send_keys(Keys.RETURN)

            time.sleep(10)

            submit2 = selenium.find_element_by_id('cancel')
            submit2.send_keys(Keys.RETURN)

            time.sleep(10)
            page = selenium.page_source

            self.assertIn(username, page)

        except NoSuchElementException:
            pass