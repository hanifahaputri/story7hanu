from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Form
from .models import NewStatus

form_data = [{}]

def index(request):
	datas = NewStatus.objects.all().order_by('-created_date')
	if request.method == 'POST':
		form = Form(request.POST)
		form_data[0]["title"] = request.POST.get("title")
		form_data[0]["status"] = request.POST.get("status")

		if form.is_valid():
			return redirect("/add_todo/")
		else:
			return render(request, 'home.html', {"datas": datas})

	else:
			return render(request, 'home.html', {"datas": datas})
			
def add_todo(request):
	if request.method == 'POST':
		form = Form(request.POST)
		if form.is_valid():
			form.save(commit=True)
			return redirect("/")
		else:
			return render(request, 'confirm.html', {"form_data" : form_data})

	else:
		return render(request, 'confirm.html', {"form_data" : form_data})
