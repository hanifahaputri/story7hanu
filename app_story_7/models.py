from django.db import models

class NewStatus(models.Model):
    title = models.CharField(max_length=27)
    status = models.CharField(max_length=200, null=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % self.title