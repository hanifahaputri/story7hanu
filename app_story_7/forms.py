from django.forms import ModelForm
from .models import NewStatus

class Form(ModelForm):
    class Meta:
        model = NewStatus
        fields = '__all__'
